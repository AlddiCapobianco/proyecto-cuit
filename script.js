function calcular() {

    let XY, Z;

    if (document.querySelector("#mas").checked) {
        XY = "20";
    } else if (document.querySelector("#fem").checked) {
        XY = "27";
    } else {
        XY = "30";
    }

let multiplicadores = [3, 2, 7, 6, 5, 4, 3, 2];
let dni = document.querySelector("#dni").value;
let calculo = ((parseInt(XY.charAt(0)) * 5) + (parseInt(XY.charAt(1)) * 4));

for(let i=0;i<8;i++) {
    calculo += (parseInt(dni.charAt(i)) * multiplicadores[i]);
}

let resto = (parseInt(calculo)) % 11;

if(document.querySelector("#emp").checked & resto === 1){
    if(document.querySelector("#mas").checked){
        Z = 9;
    } else {
        Z = 4;
    }
    XY = '23';
} else if(resto === 0){
    Z = 0;
} else {
    Z = 11 - resto;
}

document.querySelector("#resultado").innerHTML = ("El número de CUIT/CUIL es: ") + XY + ("-") + dni + ("-") + Z;
}